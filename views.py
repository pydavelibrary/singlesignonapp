# -*- coding: utf-8 -*-

# import urllib.request

from django.contrib.auth import authenticate, login, logout
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.conf import settings

from .forms import UserForm
import requests



def use_sso():
    SSO_URL = getattr(settings, 'SSO_URL', None)
    return bool(SSO_URL)


def can_register():
    return bool(getattr(settings, 'CAN_REGISTER', False))


def parse_sso(user_json):
    SSO_STAFF_LEVEL = getattr(settings, 'SSO_STAFF_LEVEL', 90)

    username     = user_json['username']
    is_staff     = (user_json['level'] >= SSO_STAFF_LEVEL)
    is_superuser = (user_json['level'] >= 99)
    user         = User.objects.filter(username=username).first()
    if not user:
        user = User(username=username)
    user.first_name   = user_json['nickname']
    user.is_superuser = is_superuser
    user.is_staff     = is_staff
    user.save()
    return user


def login_wo_authen(request, user):
    """
    Log in a user without requiring credentials (using ``login`` from
    ``django.contrib.auth``, first finding a matching backend).

    """
    from django.contrib.auth import load_backend, login

    if not hasattr(user, 'backend'):
        for backend in settings.AUTHENTICATION_BACKENDS:
            if user == load_backend(backend).get_user(user.pk):
                user.backend = backend
                break

    if not hasattr(user, 'backend'):
        user.backend = 'django.contrib.auth.backends.ModelBackend'

    if hasattr(user, 'backend'):
        return login(request, user)


def custom_redirect(url, params):
    try:
        from urllib import urlencode  # Python 2
    except ImportError:
        from urllib.parse import urlencode  # Python 3

    params = urlencode(params) # Python 3
    return HttpResponseRedirect(url + "?%s" % params)


@csrf_exempt
def login_sso(request):
    if not use_sso():
        return render(request, 'login_sso_disable.html')
    else:
        token = request.GET.get('token', None)
        if not token:
            return render(request, 'login_sso_failure.html')
        r = requests.get(settings.SSO_URL + '/accounts/api/', params={'token': token})
        data = r.json()
        if not data['success']:
            return render(request, 'login_sso_failure.html')
        user = parse_sso(data['user'])
        login_wo_authen(request, user)
        next_url = request.GET.get('next_url', '/')
        return HttpResponseRedirect(next_url)


@csrf_exempt
def login_user(request):
    if use_sso():
        SSO_SHOW_CONFIRM = getattr(settings, 'SSO_SHOW_CONFIRM', True)
        callback_url = request.build_absolute_uri(reverse('accounts.login_sso'))
        next_url = request.GET.get('next', '/')
        next_url = request.build_absolute_uri(next_url)
        url = '%s%s' % (settings.SSO_URL, '/accounts/sso/')
        APPS_NAME = getattr(settings, 'APPS_NAME', None)
        return custom_redirect(url, {
            'next_url': next_url,
            'callback_url': callback_url,
            'show_confirm': SSO_SHOW_CONFIRM,
            'apps_name': APPS_NAME,
        })
    else:
        logout(request)
        username = password = ''
        if request.POST:
            username = request.POST['username']
            password = request.POST['password']

            user = authenticate(username=username, password=password)
            if user is not None:
                if user.is_active:
                    login(request, user)
                    next_url = request.GET.get('next', '/')
                    return HttpResponseRedirect(next_url)
        data = {
            'USE_SOCIAL_AUTH': getattr(settings, 'USE_SOCIAL_AUTH', [])
        }
        return render(request, 'login.html', data)


def register(request):
    if use_sso() or not can_register():
        return render(request, 'register_disable.html')

    if request.method == 'POST':
        form = UserForm(request.POST)
        if form.is_valid():
            form_data = form.save(commit=False)
            user = User.objects.create_user(username=form_data.username, password=form_data.password)
            user = authenticate(username=form_data.username, password=form_data.password)
            login(request, user)
            next_url = request.GET.get('next', '/')
            return HttpResponseRedirect(next_url)
    else:
        form = UserForm()
    data = {
        'form': form,
    }
    return render(request, 'register.html', data)
