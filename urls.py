from django.conf.urls import include, url
from django.views.generic import RedirectView, TemplateView
from django.contrib.auth.views import logout
from . import views

urlpatterns = [
    url(r'^login/$', views.login_user, name="accounts.login"),
    url(r'^register/$', views.register, name="accounts.register"),
    url(r'^logout/$', logout, {'next_page': '/'}, name="accounts.logout"),
    url(r'^login_sso/$', views.login_sso, name="accounts.login_sso"),
    url(r'^profile/$', RedirectView.as_view(url='/', permanent=False)),
]